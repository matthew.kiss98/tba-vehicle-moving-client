# Vehicle Simulator Project > Frontend component

## Prerequisites 

In order to successfully run the application, the following requirements are needed:

- [Java 8](https://www.oracle.com/java/technologies/javase-downloads.html) or later
- [Node](https://nodejs.org/)


## How to Launch the App

### 1. Make sure you cloned both the [backend](https://gitlab.com/matthew.kiss98/tba-vehicle-moving) and frontend part of the project.

### 2. Launch the backend part.

- Navigate to the backend project root locally. 

- In the command prompt, run the `gradlew build` command. 

    _(**Hint**: You can easily open a command prompt in the folder you are in by clicking on the navigation bar, typing `cmd` then pressing Enter.)_

- Wait until build finishes.

- In the project folder, navigate to `vehicle-web/build/libs` folder.

- Run the `java -jar vehicle-web-1.0-SNAPSHOT.jar` command in the command prompt.

- After a few seconds the Spring logo should appear and the backend server should be running now.

### 3. Launch the frontend part.

- After launching the backend server, navigate to the frontend project root locally.

- In the command prompt, run the following commands: 

    `npm install` - installs dependencies
    
    `npm run build` - creates an optimized production build

    `npm install -g serve` - installs a static server

    `serve -s build` - creates and launches the static server

- By this time, the address of the server should be logged to the consol - by default `http://localhost:5000`.

- Open a browser and navigate to the server's address.

### 4. Enjoy the app!

Congratulations! The simulator should be running and working now. Feel free to try it!




