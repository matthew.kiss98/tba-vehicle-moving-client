import React from 'react';
import './style/App.css';
import Board from './components/Board';
import FormsModule from './components/FormsModule';

function App() {
  return (
    <div>
      <FormsModule />
      <Board />
    </div>
  );
}

export default App;
