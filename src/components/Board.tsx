import React, { useEffect } from 'react';
import axios from 'axios';
import useImage from 'use-image';
import { Stage, Layer } from 'react-konva';
import Car from './Car'

const Board = () => {
  const [vehicles, setVehicles] = React.useState([]);
  const [image] = useImage('https://icon-library.com/images/top-down-car-icon/top-down-car-icon-8.jpg');

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get(
        'http://localhost:8080/vehicles/'
      );
      setVehicles(result.data);
    };

    setInterval(fetchData, 2000);
  }, []);

  return (
    <Stage width={window.innerWidth - 390} height={window.innerHeight}>
      <Layer>
        {vehicles.map((vehicle: any) => (
          <Car
            key={vehicle.vehicleId}
            id={vehicle.vehicleId}
            x={vehicle.destination.x}
            y={vehicle.destination.y}
            image={image}
          />
        ))}
      </Layer>
    </Stage>
  );
};

export default Board;
