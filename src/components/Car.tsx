import React, { Component } from 'react';
import { Image, Group, Text } from "react-konva";

interface CarProps {
  x: number,
  y: number,
  id: number,
  image: any
}

interface CarState {
  currentPosition: {
    x: number,
    y: number,
  }
  destination: {
    x: number,
    y: number,
  }
  id: number
  direction: number,
}

class Car extends Component<CarProps, CarState> {

  constructor(props: any) {
    super(props);
    this.state = {
      id: 0,
      currentPosition: {
        x: 10,
        y: 300,
      },
      destination: {
        x: 10,
        y: 300,
      },
      direction: 0,
    }
  }

  componentDidMount() {
    this.setState(
      this.refreshPositions({
        ...this.state,
        destination: {
          x: this.props.x ? this.props.x : this.state.destination.x,
          y: this.props.y ? this.props.y : this.state.destination.y,
        }
      })
    )
    setInterval(this.update.bind(this), 30);
  }

  update() {
    this.setState(
      this.refreshPositions({
        ...this.props,
        currentPosition: {
          x: this.state.currentPosition.x,
          y: this.state.currentPosition.y,
        },
        destination: {
          x: this.props.x ? this.props.x : this.state.destination.x,
          y: this.props.y ? this.props.y : this.state.destination.y,
        }
      })
    )
  }

  refreshPositions(car: any): CarState {
    const step = 2
    if (car.destination.x > car.currentPosition.x) {
      car.currentPosition.x += step;
    }

    if (car.destination.y > car.currentPosition.y) {
      car.currentPosition.y += step;
    }

    if (car.destination.x < car.currentPosition.x) {
      car.currentPosition.x -= step;
    }

    if (car.destination.y < car.currentPosition.y) {
      car.currentPosition.y -= step;
    }

    return {
      ...car
    }
  }

  render() {
    return (
      <Group>
        <Image class="car"
          image={this.props.image}
          x={this.state.currentPosition.x}
          y={this.state.currentPosition.y}
          scale={{ x: 0.7, y: 0.7 }}
        />
        <Text class="caption"
          text={`${this.props.id}`}
          fontSize={32}
          x={this.state.currentPosition.x + 20}
          y={this.state.currentPosition.y + 75}
        />
      </Group>
    );
  }
}

export default Car;
