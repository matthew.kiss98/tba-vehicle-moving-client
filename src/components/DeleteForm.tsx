import React from 'react';
import { useForm } from "react-hook-form";
import axios from 'axios';

const DeleteForm = () => {
  const { register, handleSubmit } = useForm();

  const onSubmit = async (data: any) => {
    console.log(data);

    await axios.delete(
      `http://localhost:8080/vehicles/${data.vehicleId}`,
      data
    )
  };

  const deleteAll = async () => {
    await axios.delete(
      'http://localhost:8080/vehicles'
    );
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <h1>Delete vehicle</h1>
      <input name="vehicleId"
        type="number"
        ref={register({ required: true, min: 1 })}
        min={1}
        placeholder="Enter vehicle ID" />
      <div className="buttonRow">
        <button type="button" className="reset" onClick={deleteAll}>Delete all</button>
        <button type="submit">Delete</button>
      </div>
    </form >
  )

}

export default DeleteForm;
