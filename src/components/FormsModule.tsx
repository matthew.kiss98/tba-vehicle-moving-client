import React from 'react';
import ManipulateForm from './ManipulateForm';
import DeleteForm from './DeleteForm';
import InsertForm from './InsertForm';

const FormsModule = () => {
  return (
    <span>
      <InsertForm />
      <ManipulateForm />
      <DeleteForm />
    </span>
  );
}

export default FormsModule;
