import React from 'react';
import { useForm } from "react-hook-form";
import axios from 'axios';
import { Label } from 'react-konva';

const InsertForm = () => {
  const { register, handleSubmit, errors } = useForm();

  const onSubmit = async (data: any) => {
    console.log(data);

    await axios.post(
      'http://localhost:8080/vehicles',
      data
    );
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <h1>Insert new vehicle</h1>
      <Label>Enter new vehicle's destination coordinates:</Label>
      <div className="coordinate">
        <input name="dest.x"
          type="number"
          ref={register({ required: true, min: 1, max: window.innerWidth - 550 })}
          min={1}
          placeholder="X" />
        <input name="dest.y"
          type="number"
          ref={register({ required: true, min: 1, max: window.innerHeight - 70 })}
          min={1}
          placeholder="Y" />
      </div>
      {errors.dest && errors.dest.x && <span className="error">The X value is blank or too high.</span>}
      {errors.dest && errors.dest.y && <span className="error">The Y value is blank or too high.</span>}
      <div className="buttonRow">
        <button type="reset" className="reset">Reset fields</button>
        <button type="submit">Insert Vehicle</button>
      </div>
    </form>
  )

}

export default InsertForm;
