import React from 'react';
import { useForm } from "react-hook-form";
import axios from 'axios';
import { Label } from 'react-konva';

const ManipulateForm = () => {
  const { register, handleSubmit, errors } = useForm();

  const onSubmit = async (data: any) => {
    console.log(data);

    await axios.put(
      `http://localhost:8080/vehicles/${data.vehicleId}`,
      data
    )
  };

  return (
    <form
      id="manipulate-form"
      onSubmit={handleSubmit(onSubmit)}>
      <h1>Manipulate vehicle's motion</h1>
      <input name="vehicleId"
        type="number"
        ref={register({ required: true, min: 1 })}
        min={1}
        placeholder="Enter vehicle ID" />
      <Label>Enter vehicle's new destination coordinates:</Label>
      <div className="coordinate">
        <input name="dest.x"
          type="number"
          ref={register({ required: true, min: 1, max: window.innerWidth - 550 })}
          min={1}
          placeholder="X" />
        <input name="dest.y"
          type="number"
          ref={register({ required: true, min: 1, max: window.innerHeight - 70 })}
          min={1}
          placeholder="Y" />
      </div>
      {errors.dest && errors.dest.x && <span className="error">The X value is blank or too high.</span>}
      {errors.dest && errors.dest.y && <span className="error">The Y value is blank or too high.</span>}
      <div className="buttonRow">
        <button type="reset" className="reset">Reset fields</button>
        <button type="submit">Manipulate</button>
      </div>
    </form>
  )

}

export default ManipulateForm;
